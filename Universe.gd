extends Node2D

onready var asteroidDir := $Asteroids
onready var asteroidTmpl := preload("res://Asteroid.tscn")
export var maxXY := Vector2(10000, 10000)

func asteroid_generator():
	var asteroid : Asteroid = asteroidTmpl.instance()
	asteroid.spriteIdx = randi() % 8
	$Asteroids.add_child(asteroid)
	asteroid.position = Vector2(
		randi() % int(maxXY.x),
		randi() % int(maxXY.y)
	)
	asteroid.spriteIdx = randi() % 8

func _ready() -> void:
	$Player/Camera2D.limit_right = maxXY.x
	$Player/Camera2D.limit_bottom = maxXY.y
	for i in 25:
		asteroid_generator()
