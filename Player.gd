extends KinematicBody2D

export var max_speed = 500
export var boost_speed = 1000

var collectedAsteroids := 0
var direction := Vector2.ZERO
var speed = max_speed  # Pixely za sekundu
var velocity:= Vector2.ZERO

onready var timer := $Timer
onready var collectedAsteroidsControl := $CanvasLayer/PocetAsteroidu

func _physics_process(delta: float) -> void:
	direction = Input.get_vector("move_left","move_right","move_up","move_down")
	if Input.is_action_just_pressed("move_boost"):
		speed = boost_speed
		timer.start()
		
	velocity = direction * speed * delta
	move_and_collide(velocity)
	
	if direction:
		rotation = direction.angle()

func _on_Timer_timeout() -> void:
	speed = max_speed

func _on_Collector_area_entered(area) -> void:
	if area is Asteroid:
		area.free()
		collectedAsteroids += 1
		collectedAsteroidsControl.text = "Sesbíráno: " + str(collectedAsteroids)

func _on_Collector_body_entered(body: Node) -> void:
	print(body.get_collision_layer_bit(1))
	if body is TileMap:
		print(body.to_string())
