tool

class_name Asteroid extends Area2D

onready var sprite = $Sprite
onready var shapes = [$CollisionShape2D, $CollisionShape2D2, $CollisionShape2D3, $CollisionShape2D4]

var spriteIdx = 0

#func _init(spriteIdx) -> void:
#	spriteIdx = spriteIdx

func _ready() -> void:
	connect("body_entered", self, "_on_body_entered")
	sprite.frame = spriteIdx
	var spriteShape : CollisionShape2D = shapes[sprite.frame_coords.x]
	spriteShape.disabled = false

func _process(delta: float) -> void:
	if Engine.editor_hint:
		sprite.frame = spriteIdx
